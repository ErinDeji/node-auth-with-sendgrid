const express = require('express')
const morgan = require('morgan')
const cors = require('cors')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')

require('dotenv').config()


const app = express()

// connect to mongo
mongoose
    .connect(
        process.env.DATABASE, {
        useUnifiedTopology: true,
        useFindAndModify: false,
        useNewUrlParser: true,
        useCreateIndex: true
    }
    )
    .then(() => console.log("Connected to mongo instance"))
    .catch((err) => {
        console.log(err.message);
    });

// import routes
const authRoutes = require('./routes/auth')

// app middlewares
app.use(morgan('dev'))
app.use(bodyParser.json())
app.use(cors())
if ((process.env.NODE_ENV = 'development')) {
    app.use(cors({ origin: `http://localhost:4000` }))
}

app.use('/api', authRoutes)

const port = process.env.PORT || 9000
app.listen(port, () => {
    console.log(`running on port ${port}`);
})