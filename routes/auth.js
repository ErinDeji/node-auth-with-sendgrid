const express = require('express')
const router = express.Router()

// import contoller
const { signup, signin, accountActivation } = require('../controllers/auth')

// import validators
const { userSignupValidator, userSigninValidator } = require('../validators/auth')
const { runValidation } = require('../validators/index')


router.post('/signup', userSignupValidator, runValidation, signup)
router.post('/account-activation', accountActivation, runValidation, signup)
router.post('/signin', userSigninValidator, runValidation, signin)

module.exports = router