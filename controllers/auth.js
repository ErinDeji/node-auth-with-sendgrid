const User = require('../models/User')
const jwt = require('jsonwebtoken')
const sgMail = require('@sendgrid/mail')
sgMail.setApiKey(process.env.SENDGRID_API_KEY)


exports.signup = (req, res) => {
    const { name, email, password } = req.body

    User.findOne({ email }).exec((err, user) => {
        if (user) {
            return res.status(400).json({
                error: 'Email already in use by another user'
            })
        }

        const token = jwt.sign({ name, email, password }, process.env.JWT_ACCONT_ACTIVATION, { expiresIn: '10m' })

        const emailData = {
            from: process.env.EMAIL_FROM,
            to: email,
            subject: `Account activation link`,
            html: `
            <h1> please use the link to activate your account</h1>
            <h4>${process.env.CLIENT_URL}/auth/activate/${token}</h4>
            <h3>This email may contain subsentive information</h3>
            <h4>${process.env.CLIENT_URL}</h4>
            `
        }
        sgMail.send(emailData).then(() => {
            return res.json({
                message: `email has been sent to ${email}. Follow the instructions to activate your account`
            })
        }).catch((err) => {
            return res.json({
                message: err.message
            })
        })
    })

}


exports.accountActivation = (req, res) => {
    const { token } = req.body
    if (token) {
        jwt.verify(token, process.env.JWT_ACCONT_ACTIVATION, function (err, decoded) {
            if (err) {
                console.log('JWT VERIFY IN ACCOUNT ACTIVATION ERROR', err);
                return res.status(401).json({
                    error: 'Expired Link. Signup again'
                })
            }
            const { name, email, password } = jwt.decode(token)

            const user = new User({ name, email, password })

            user.save((err, user) => {
                if (err) {
                    console.log('SAVE USER IN ACCOUNT ERROR', err);
                    return res.status(401).json({
                        error: 'Error saving user in database. Sign up again'
                    })
                }
                return res.json({
                    message: 'Signup success. Please sign in'
                })
            })
        })
    } else {
        return res.json({
            message: 'Something went wrong. Try again'
        })
    }
}

exports.signin = (req, res) => {
    const { email, password } = req.body

    User.findOne({ email }).exec((err, user) => {
        if (err || !user) {
            return res.status(400).json({
                error: 'User with that email does not exist. Please signup'
            })
        }
        if (!user.authenticate(password)) {
            return res.status(400).json({
                error: 'Email and password does not match'
            })
        }
        const token = jwt.sign({ _id: user._id }, process.env.JWT_SECRET, { expiresIn: '7d' })

        const { _id, name, email, role } = user

        return res.json({
            token,
            user: { _id, name, email, role }
        })
    })
}
